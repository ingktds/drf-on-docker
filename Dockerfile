FROM python:3.12.3
LABEL maintainer="tadashi.1027@gmail.com"

WORKDIR /app
VOLUME [ "/app" ]
ADD Pipfile* .
RUN apt update && apt -y upgrade
RUN pip install --upgrade pip
RUN pip install pipenv
RUN pipenv install

EXPOSE 8000
CMD ["pipenv", "run", "python", "manage.py", "runserver", "0.0.0.0:8000"]
