# Django REST framework on Docker 

Django REST framework on Docker is to develop an environment using Django REST framework easily and quickly.

## Setup

You need to migrate database and set up administrative user on Django.

```bash
cd /path/to/drf-on-docker
docker compose app up -d
docker compose exec app bash
pipenv shell
python manage.py migrate
python manage.py createsuperuser --username admin --email youremail@hoge.com
```

## Usage

Describe how to start and stop this application.

### How to start

```bash
docker compose up -d
```

You can access to [http://localhost:8000](http://localhost:8000) using your browser.

### How to stop

```bash
docker compose down
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://gitlab.com/ingktds/drf-on-docker/-/blob/main/LICENSE)
